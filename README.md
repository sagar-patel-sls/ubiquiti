# Ubiquiti

Lab and testing projects involving Ubiquiti products

Note:  Use these scripts at your own-risk!

Most of these scripts are used for some lab projects. While they will probably work,
I highly recommend you test and alter them BEFORE using them in a production environment.
